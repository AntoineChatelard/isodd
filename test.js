var assert = require('assert');
var functions = require('./index.js');

describe('Number', function() {
	describe('#isOdd()', function() {
		it('should return true when the number is odd', function() {
			assert.equal(functions.isOdd(1), true);
		});
		it('should return false when the number is not odd', function() {
			assert.equal(functions.isOdd(2), false);
		});
	});
});

